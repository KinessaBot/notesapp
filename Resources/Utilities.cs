﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace NotesApp.Resources
{
    static class Utilities
    {
        public static string ConnectionString { get; } = "Server=127.0.0.1; Port=3308; Database=notes; user=root1; password=root1;";// "DataSource=127.0.0.1; InitialCatalog=notes; Integrated Security=true;";
        public static Dictionary<int, Color> PriorityColors { get; } = new Dictionary<int, Color> {
            { 0, Color.White },
            { 1, Color.AliceBlue },
            { 2, Color.Aquamarine },
            { 3, Color.Azure },
            { 4, Color.Green },
            { 5, Color.Blue },
            { 6, Color.BlueViolet },
            { 7, Color.Violet },
            { 8, Color.MediumVioletRed },
            { 9, Color.Red }
        };
        public static int NoteElementWidth { get; } = 200;
        public static int NoteElementHeight { get; } = 100;

        /*public static int NoteElementVerticalPadding { get; } = 30;
        public static int NoteElementHorizontalPadding { get; } = 20;*/

        public static void FillColorPriorityPicker(FlowLayoutPanel flowPanel)
        {
            foreach (var pair in ClientData.PriorityColors)
            {
                var panel = new Panel();
                panel.BackColor = pair.Value;
                panel.Size = new Size(30, 30);
                panel.BorderStyle = BorderStyle.FixedSingle;
                panel.Parent = flowPanel;
                panel.Click += ColorPanelClick;
                panel.Tag = (object)pair.Key;
                flowPanel.Controls.Add(panel);
            }
        }

        internal static int GetPriorityOfPicker(FlowLayoutPanel flowPanel)
        {
            foreach (Panel panel in flowPanel.Controls)
            {
                if (panel.BorderStyle == BorderStyle.Fixed3D)
                {
                    return (int)panel.Tag;
                }
            }
            throw new Exception("Priority didnt picked");
        }

        private static void ColorPanelClick(object sender, EventArgs e)
        {
            foreach (Panel panel in ((Panel)sender).Parent.Controls)
            {
                panel.BorderStyle = BorderStyle.FixedSingle;
            }
            ((Panel)sender).BorderStyle = BorderStyle.Fixed3D;
        }
    }
}
            
