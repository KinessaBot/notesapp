﻿using Newtonsoft.Json;
using NotesApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace NotesApp.Resources
{
    [Serializable]
    public static class ClientData
    {
        [NonSerialized]
        public static long id;
        [NonSerialized]
        public static Context context;
        public static string ConnectionString = Utilities.ConnectionString;
        public static int NoteElementWidth = Utilities.NoteElementWidth;
        public static int NoteElementHeight = Utilities.NoteElementHeight;
        /*public static int NoteElementVerticalPadding = Utilities.NoteElementVerticalPadding;
        public static int NoteElementHorizontalPadding = Utilities.NoteElementHorizontalPadding;*/
        public static Dictionary<int, Color> PriorityColors = Utilities.PriorityColors;


        private class SaveJson
        {
            public string ConnectionString;
            public int NoteElementWidth;
            public  int NoteElementHeight;
            /*public static int NoteElementVerticalPadding;
            public static int NoteElementHorizontalPadding;*/
            public static Dictionary<int, Color> PriorityColors;

            public SaveJson()
            {
                ConnectionString = ClientData.ConnectionString;
                NoteElementWidth = ClientData.NoteElementWidth;
                NoteElementHeight = ClientData.NoteElementHeight;
                PriorityColors = ClientData.PriorityColors;
                // -- HERE CAN BE PADDING SETTINGS-- //
            }
        }

        public static void SaveToJson()
        {
            File.WriteAllText(@"settings.json", JsonConvert.SerializeObject(new SaveJson()));
        }

        public static void LoadFromJson()
        {
            using (StreamReader r = new StreamReader("settings.json"))
            {
                string json = r.ReadToEnd();
                SaveJson save = JsonConvert.DeserializeObject<SaveJson>(json);
            }
        }
    }
}
