﻿using NotesApp.Forms;
using NotesApp.models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotesApp.Resources
{
    public class NoteElement : GroupBox
    {
        public NoteElement(Note note)
        {
            Color bgColor;
            ClientData.PriorityColors.TryGetValue(note.Priority, out bgColor);
            this.BackColor = bgColor;
            Width = ClientData.NoteElementWidth;
            Height = ClientData.NoteElementHeight;


            Label label = new Label();
            label.Text = note.Header;
            label.Size = label.PreferredSize;
            label.Location = new Point(this.Width / 2 - label.Width / 2, 10);


            Button button = new Button();
            button.Text = "details";
            button.Click += (s,e) => new DetailsForm(note).Show();
            button.Location = new Point(this.Width / 2 - button.Width / 2, this.Height - 30);

            this.Controls.Add(label);
            this.Controls.Add(button);

            this.Refresh();
        }

    }
}
