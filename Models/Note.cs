﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotesApp.models
{
     public abstract class Note
    {
        public int Id { get; set; }
        public int Priority { get; set; }
        public string User { get; set; }
        public int User_id { get; set; }
        public string Text { get; set; }
        public string Header { get; set; }
    }
}
