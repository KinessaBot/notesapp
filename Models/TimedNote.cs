﻿using NotesApp.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotesApp.Models
{
    public class TimedNote : Note
    {
        public DateTime Time { get; set; }

        public TimeSpan TimeToGo()
        {
             return Time.Subtract(DateTime.Now);
        }
    }
}
