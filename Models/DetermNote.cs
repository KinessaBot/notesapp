﻿using NotesApp.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotesApp.Models
{
    public class DetermNote: Note
    {
        public string Place { get; set; }
    }
}
