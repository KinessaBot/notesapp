﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MySql.Data.MySqlClient;
using NotesApp.Resources;
using MySql.Data.Entity;

namespace NotesApp.Models
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class Context: DbContext
    {
        public Context() : base("name = DBConnection")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<TimedNote> TimedNotes { get; set; }
        public DbSet<DetermNote> DetermNotes { get; set; }
    }
}
