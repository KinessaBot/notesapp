﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;


namespace NotesApp.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public ICollection<DetermNote> DetermNotes { get; set; }
        public ICollection<TimedNote> TimedNotes { get; set; }
    }
}
