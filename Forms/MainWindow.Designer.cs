﻿namespace NotesApp.Forms
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.User = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.filter = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllNotes = new System.Windows.Forms.ToolStripMenuItem();
            this.showDetermNotes = new System.Windows.Forms.ToolStripMenuItem();
            this.showTimedNotes = new System.Windows.Forms.ToolStripMenuItem();
            this.addNote = new System.Windows.Forms.ToolStripMenuItem();
            this.sortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ascendingPriorityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descendingPriorityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nearestEventsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.farestEventsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.occuredEventsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // User
            // 
            this.User.AutoSize = true;
            this.User.Location = new System.Drawing.Point(707, 9);
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(35, 13);
            this.User.TabIndex = 0;
            this.User.Text = "label1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.filter,
            this.addNote,
            this.sortToolStripMenuItem,
            this.Refresh,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(57, 20);
            this.toolStripMenuItem1.Text = "log out";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // filter
            // 
            this.filter.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showAllNotes,
            this.showDetermNotes,
            this.showTimedNotes});
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(43, 20);
            this.filter.Text = "filter";
            // 
            // showAllNotes
            // 
            this.showAllNotes.Name = "showAllNotes";
            this.showAllNotes.Size = new System.Drawing.Size(208, 22);
            this.showAllNotes.Text = "show all notes";
            this.showAllNotes.Click += new System.EventHandler(this.showAllNotes_Click);
            // 
            // showDetermNotes
            // 
            this.showDetermNotes.Name = "showDetermNotes";
            this.showDetermNotes.Size = new System.Drawing.Size(208, 22);
            this.showDetermNotes.Text = "show determinated notes";
            this.showDetermNotes.Click += new System.EventHandler(this.showDetermNotes_Click);
            // 
            // showTimedNotes
            // 
            this.showTimedNotes.Name = "showTimedNotes";
            this.showTimedNotes.Size = new System.Drawing.Size(208, 22);
            this.showTimedNotes.Text = "show timed notes";
            this.showTimedNotes.Click += new System.EventHandler(this.showTimedNotes_Click);
            // 
            // addNote
            // 
            this.addNote.Name = "addNote";
            this.addNote.Size = new System.Drawing.Size(66, 20);
            this.addNote.Text = "add note";
            this.addNote.Click += new System.EventHandler(this.addNote_Click);
            // 
            // sortToolStripMenuItem
            // 
            this.sortToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ascendingPriorityToolStripMenuItem,
            this.descendingPriorityToolStripMenuItem,
            this.nearestEventsToolStripMenuItem,
            this.farestEventsToolStripMenuItem,
            this.occuredEventsToolStripMenuItem});
            this.sortToolStripMenuItem.Name = "sortToolStripMenuItem";
            this.sortToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.sortToolStripMenuItem.Text = "sort";
            // 
            // ascendingPriorityToolStripMenuItem
            // 
            this.ascendingPriorityToolStripMenuItem.Name = "ascendingPriorityToolStripMenuItem";
            this.ascendingPriorityToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.ascendingPriorityToolStripMenuItem.Text = "ascending priority";
            this.ascendingPriorityToolStripMenuItem.Click += new System.EventHandler(this.ascendingPriorityToolStripMenuItem_Click);
            // 
            // descendingPriorityToolStripMenuItem
            // 
            this.descendingPriorityToolStripMenuItem.Name = "descendingPriorityToolStripMenuItem";
            this.descendingPriorityToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.descendingPriorityToolStripMenuItem.Text = "descending priority";
            this.descendingPriorityToolStripMenuItem.Click += new System.EventHandler(this.descendingPriorityToolStripMenuItem_Click);
            // 
            // nearestEventsToolStripMenuItem
            // 
            this.nearestEventsToolStripMenuItem.Name = "nearestEventsToolStripMenuItem";
            this.nearestEventsToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.nearestEventsToolStripMenuItem.Text = "nearest events";
            this.nearestEventsToolStripMenuItem.Click += new System.EventHandler(this.nearestEventsToolStripMenuItem_Click);
            // 
            // farestEventsToolStripMenuItem
            // 
            this.farestEventsToolStripMenuItem.Name = "farestEventsToolStripMenuItem";
            this.farestEventsToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.farestEventsToolStripMenuItem.Text = "farest events";
            this.farestEventsToolStripMenuItem.Click += new System.EventHandler(this.farestEventsToolStripMenuItem_Click);
            // 
            // occuredEventsToolStripMenuItem
            // 
            this.occuredEventsToolStripMenuItem.Name = "occuredEventsToolStripMenuItem";
            this.occuredEventsToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.occuredEventsToolStripMenuItem.Text = "occured events";
            this.occuredEventsToolStripMenuItem.Click += new System.EventHandler(this.occuredEventsToolStripMenuItem_Click);
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.Location = new System.Drawing.Point(0, 25);
            this.flowLayoutPanel.MaximumSize = new System.Drawing.Size(1920, 1080);
            this.flowLayoutPanel.MinimumSize = new System.Drawing.Size(600, 400);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(800, 400);
            this.flowLayoutPanel.TabIndex = 4;
            // 
            // Refresh
            // 
            this.Refresh.Image = ((System.Drawing.Image)(resources.GetObject("Refresh.Image")));
            this.Refresh.Name = "Refresh";
            this.Refresh.Size = new System.Drawing.Size(28, 20);
            this.Refresh.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.settingsToolStripMenuItem.Text = "settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.User);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "Main window";
            this.Activated += new System.EventHandler(this.MainWindow_Enter);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.Resize += new System.EventHandler(this.MainWindow_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label User;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.ToolStripMenuItem filter;
        private System.Windows.Forms.ToolStripMenuItem showAllNotes;
        private System.Windows.Forms.ToolStripMenuItem showDetermNotes;
        private System.Windows.Forms.ToolStripMenuItem showTimedNotes;
        private System.Windows.Forms.ToolStripMenuItem addNote;
        private System.Windows.Forms.ToolStripMenuItem sortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ascendingPriorityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descendingPriorityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nearestEventsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem farestEventsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem occuredEventsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Refresh;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
    }
}