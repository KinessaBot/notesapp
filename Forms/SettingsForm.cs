﻿using NotesApp.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotesApp.Forms
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            Utilities.FillColorPriorityPicker(flowLayoutPanel1);
            foreach (Panel panel in flowLayoutPanel1.Controls)
            {
                panel.Click += ChangeColor;
            }
            textBox1.Text = ClientData.ConnectionString;
            trackBarHeights.Value = ClientData.NoteElementHeight;
            trackBarWidth.Value = ClientData.NoteElementWidth;
        }

        private void ChangeColor(object sender, EventArgs e)
        {
            Panel panel = (Panel)sender;
            int colorKey = (int)panel.Tag;
            colorDialog1.ShowDialog();
            ClientData.PriorityColors.Remove(colorKey);
            ClientData.PriorityColors.Add(colorKey, colorDialog1.Color);
            ((Panel)sender).BackColor = colorDialog1.Color;
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClientData.SaveToJson();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void trackBarHeights_Scroll(object sender, EventArgs e)
        {
            ClientData.NoteElementHeight = trackBarHeights.Value;
        }

        private void trackBarWidth_Scroll(object sender, EventArgs e)
        {
            ClientData.NoteElementWidth = trackBarWidth.Value;
        }
    }
}
