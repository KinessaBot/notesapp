﻿using NotesApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotesApp.Forms
{
    public partial class PreAddForm : Form
    {
        public PreAddForm()
        {
            InitializeComponent();
        }

        private void Nextbutton_Click(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
                new DetermNoteAddForm().Show();
            else
                new TimedNoteAddForm().Show();
            Close();
        }
    }
}
