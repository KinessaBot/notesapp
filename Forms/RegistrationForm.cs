﻿using MySql.Data.MySqlClient;
using NotesApp.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotesApp.Forms
{
    public partial class RegistrationForm : Form
    {
        public RegistrationForm()
        {
            InitializeComponent();
        }


        private void OK_Click(object sender, EventArgs e)
        {
            using (var sqlConn = new MySqlConnection(Utilities.ConnectionString))
            {
                if (Password.Text != Password2.Text)
                {
                    new Error("Passwords are not the same").ShowDialog();
                    return;
                }
                var sqlCmd = new MySqlCommand("Insert into Users(Login, Password) values(@Login, @Password)", sqlConn);
                sqlCmd.Parameters.AddWithValue("@Login", Login.Text);
                sqlCmd.Parameters.AddWithValue("@Password", Password.Text); //hash?
                sqlConn.Open();
                try
                {
                    sqlCmd.ExecuteNonQuery();
                    Close();
                }
                catch(SqlException)
                {
                    new Error("This login already exists").ShowDialog();
                }
            }
        }
    }
}
