﻿using Microsoft.EntityFrameworkCore.Internal;
using NotesApp.models;
using NotesApp.Models;
using NotesApp.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace NotesApp.Forms
{
    public partial class MainWindow : Form
    {
        public Form caller;
        private List<Note> notes;
        delegate void FormRefresh(object sender, EventArgs e);
        FormRefresh myRefresh;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            User.Text = ClientData.context.Users.First().Login;
           
            notes = new List<Note>();
            myRefresh = showAllNotes_Click;
            showAllNotes_Click(sender, e);
            flowLayoutPanel.Size = this.Size;
            flowLayoutPanel.PerformLayout();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            caller.Show();
            Close();
        }

        private void MainWindow_Resize(object sender, EventArgs e)
        {
            flowLayoutPanel.Size = this.Size;
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            caller.Close();
        }

        private void showAllNotes_Click(object sender, EventArgs e)
        {
            ClearNotes();
            myRefresh = showAllNotes_Click;
            notes.AddRange(ClientData.context.TimedNotes.Where(b => b.User_id == ClientData.id)
                                                           .OrderByDescending(b => b.Priority));
            notes.AddRange(ClientData.context.DetermNotes.Where(b => b.User_id == ClientData.id)
                                                         .OrderByDescending(b => b.Priority));
            AddNotesToFlowLayoutPanel();
        }

        private void showDetermNotes_Click(object sender, EventArgs e)
        {
            ClearNotes();
            myRefresh = showDetermNotes_Click;
            notes.AddRange(ClientData.context.DetermNotes.Where(b => b.User_id == ClientData.id)
                                                         .OrderByDescending(b => b.Priority));
            AddNotesToFlowLayoutPanel();
        }

        private void showTimedNotes_Click(object sender, EventArgs e)
        {
            ClearNotes();
            myRefresh = showTimedNotes_Click;
            notes.AddRange(ClientData.context.TimedNotes.Where(b => b.User_id == ClientData.id)
                                                           .OrderByDescending(b => b.Priority));
            AddNotesToFlowLayoutPanel();
        }

        private void AddNotesToFlowLayoutPanel()
        {
            foreach (Note note in notes)
            {
                NoteElement noteControl = new NoteElement(note);
                flowLayoutPanel.Controls.Add(noteControl);
            }
        }

        private void ClearNotes()
        {
            flowLayoutPanel.Controls.Clear();
            notes.Clear();
        }

        private void addNote_Click(object sender, EventArgs e)
        {
            new PreAddForm().Show();
        }

        private void ascendingPriorityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRefresh = (FormRefresh)myRefresh.GetInvocationList().First();
            myRefresh += ascendingPriorityToolStripMenuItem_Click;
            notes.OrderBy(b => b.Priority);
            flowLayoutPanel.Controls.Clear();
            AddNotesToFlowLayoutPanel();
        }

        private void descendingPriorityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRefresh = (FormRefresh)myRefresh.GetInvocationList().First();
            myRefresh += descendingPriorityToolStripMenuItem_Click;
            notes.OrderByDescending(b => b.Priority);
            flowLayoutPanel.Controls.Clear();
            AddNotesToFlowLayoutPanel();
        }

        private void nearestEventsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRefresh = (FormRefresh)myRefresh.GetInvocationList().First();
            myRefresh += nearestEventsToolStripMenuItem_Click;
            ClearNotes();
            notes.AddRange(ClientData.context.TimedNotes.Where(b=> b.User_id == ClientData.id && b.Time > DateTime.Now)
                                                        .OrderBy(b =>  EntityFunctions.DiffSeconds(b.Time, DateTime.Now)));
            AddNotesToFlowLayoutPanel();
        }

        private void farestEventsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRefresh = (FormRefresh)myRefresh.GetInvocationList().First();
            myRefresh += farestEventsToolStripMenuItem_Click;
            ClearNotes();
            notes.AddRange(ClientData.context.TimedNotes.Where(b => b.User_id == ClientData.id && b.Time > DateTime.Now)
                                                        .OrderByDescending(b => EntityFunctions.DiffSeconds(b.Time, DateTime.Now)));
            AddNotesToFlowLayoutPanel();
        }

        private void occuredEventsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRefresh = (FormRefresh)myRefresh.GetInvocationList().First();
            myRefresh += occuredEventsToolStripMenuItem_Click;
            ClearNotes();
            notes.AddRange(ClientData.context.TimedNotes.Where(b => b.User_id == ClientData.id && b.Time < DateTime.Now));
            AddNotesToFlowLayoutPanel();
        }

        private void MainWindow_Enter(object sender, EventArgs e)
        {
            myRefresh.Invoke(sender, e);
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            myRefresh.Invoke(sender,e);
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SettingsForm().Show();
        }
    }
}
