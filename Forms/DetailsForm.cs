﻿using MySql.Data.MySqlClient;
using NotesApp.models;
using NotesApp.Models;
using NotesApp.Resources;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace NotesApp.Forms
{
    public partial class DetailsForm : Form
    {
        private Note note;
        public DetailsForm(Note note)
        {
            this.note = note;      
            InitializeComponent();
        }

        private void DetailsForm_Load(object sender, EventArgs e)
        {
            Text = note.Header;
            Color color;
            Utilities.PriorityColors.TryGetValue(note.Priority, out color);
            header.Text = note.Header;
            user.Text = note.User;
            description.Text = note.Text;
            if (note is TimedNote)
            {
                date.SetSelectionRange(((TimedNote)note).Time.Date, ((TimedNote)note).Time.Date);
                date.Enabled = false;
                time.Text = ((TimedNote)note).Time.TimeOfDay.ToString();
            }
            if (note is DetermNote)
            {
                date.Visible = false;
                time.Text = ((DetermNote)note).Place;
            }
            BackColor = color;
        }

        private void delete_Click(object sender, EventArgs e)
        {
            using (MySqlConnection sqlConn = new MySqlConnection(Utilities.ConnectionString))
            {
                var sqlCmd = new MySqlCommand("Delete from note where id=@id", sqlConn);
                sqlCmd.Parameters.AddWithValue("@id", note.Id);
                sqlConn.Open();
                if (sqlCmd.ExecuteNonQuery() == 0)
                {
                    new Error("Cant delete a note").ShowDialog();
                }
                else
                    Close();
            }
        }
    }
}
