﻿using MySql.Data.MySqlClient;
using NotesApp.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotesApp.Forms
{
    public partial class DetermNoteAddForm : Form
    {
        public DetermNoteAddForm()
        {
            InitializeComponent();
        }

        private void DetermNoteAddForm_Load(object sender, EventArgs e)
        {
            Utilities.FillColorPriorityPicker(flowLayoutPanel1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (MySqlConnection sqlConn = new MySqlConnection(Utilities.ConnectionString))
                {
                    sqlConn.Open();
                    var sqlCmd = new MySqlCommand("Insert into Note(user_id,priority,text,header,time,place) values(@id,@priority,@text,@header,NULL, @place)", sqlConn);
                    sqlCmd.Parameters.AddWithValue("@id", ClientData.id);
                    sqlCmd.Parameters.AddWithValue("@priority", Utilities.GetPriorityOfPicker(flowLayoutPanel1));
                    sqlCmd.Parameters.AddWithValue("@text", textBox.Text);
                    sqlCmd.Parameters.AddWithValue("@header", HeaderBox.Text);
                    sqlCmd.Parameters.AddWithValue("@place", PlaceBox.Text);
                    if (sqlCmd.ExecuteNonQuery() == 0)
                        new Error("An error occured on adding a note").ShowDialog();
                    else
                        Close();
                }
            }
            catch (Exception exc)
            {
                new Error(exc.Message).Show();
            }
        }
    }
}
