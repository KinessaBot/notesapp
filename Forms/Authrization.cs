﻿using NotesApp.Forms;
using NotesApp.Resources;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using NotesApp.Models;
using System.Data.Entity;

namespace NotesApp
{
    public partial class Authorization : Form
    {
        public Authorization()
        {
            InitializeComponent();
        }

        private void OkClick(object sender, EventArgs e)
        {
            using (MySqlConnection sqlConn = new MySqlConnection(Utilities.ConnectionString))
            {
                var sqlCmd = new MySqlCommand("SELECT Id FROM Users WHERE login=@login AND Password=@Password", sqlConn);
                sqlCmd.Parameters.AddWithValue("@login", Login.Text);
                sqlCmd.Parameters.AddWithValue("@Password", Password.Text); //hash?
                sqlConn.Open();
                using (var dataReader = sqlCmd.ExecuteReader())
                {
                    if (dataReader.Read())
                    {
                        ClientData.id = dataReader.GetInt64(0);
                        ClientData.context = new Context();
                        Preload();
                        MainWindow mw = new MainWindow();
                        mw.caller = this;
                        mw.Show();
                        Hide();
                    }
                    else
                        new Error("Input data is incorrect").ShowDialog();
                }
            }
        }

        private void Preload()
        {
            User user = ClientData.context.Users.Find(ClientData.id);
            ClientData.context.TimedNotes.Where(b => b.User_id == ClientData.id).Load();
            ClientData.context.DetermNotes.Where(b => b.User_id == ClientData.id).Load();
        }

        private void Registration_Click(object sender, EventArgs e)
        {
            new RegistrationForm().Show();
        }

        private void Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                OkClick(sender, e);
                e.Handled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new Error("Nothing here yet").Show();
        }
    }
}
